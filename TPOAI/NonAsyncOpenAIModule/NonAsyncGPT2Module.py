"""
NonAsyncGPT2 Module

Version 1.0
"""

from transformers import GPT2Tokenizer, TFGPT2LMHeadModel #pip install transformers
import tensorflow as tf #pip install tensorflow

class NonAsyncGPT2(object):
	
	KnownModels = [
						"gpt2",
						"gpt2-medium",
						"gpt2-large",
						"gpt2-xl",
					]
	
	Model = ""
	
	Tokeniser = None
	HeadModel = None
	
	Loaded = False
	
	ModelConfig = {
							"do_sample":True,
							"temperature":0.7,			#The value of temperature to use when generating the sequence.
							"top_k":5,					#The number of highest probability vocabulary tokens to keep for top-k-filtering.
							"top_p":0.9,				#The cumulative probability of parameter highest probability vocabulary tokens to keep for nucleus sampling.
							"repetition_penalty":1.5,	#The parameter for repetition penalty. Between 1.0 and + infinity. 1.0 means no penalty. Default to 1.0.
							"bos_token_id":None,		#he beginning of sequence token if set to a number. If None, bos_token is used instead.
							"pad_token_id":None,		#Padding token if set to a number. If None, pad_token is used instead.
							"eos_token_id":None,		#The end of sequence token if set to a number. If None, eos_token is used instead.
							"length_penalty":1.0,		#Exponential penalty to the length. Default to 1.0.
							"num_return_sequences":1	#The number of independently computed returned sequences for each input sequence.
						}
	
	def __init__(self,LoadModel="gpt2"):
		self.Model = LoadModel if (LoadModel in self.KnownModels) else self.KnownModels[0]
	
	def IsLoaded(self):
		return not self.Tokeniser is None and not self.LoadModel is None
	
	def LoadTokeniser(self,Model):
		self.Tokeniser = GPT2Tokenizer.from_pretrained(Model)
		
	def LoadHeadModel(self,Model):
		self.HeadModel = TFGPT2LMHeadModel.from_pretrained(Model)
		
	def LoadModel(self,Model):
		self.LoadTokeniser(Model)
		self.LoadHeadModel(Model)

	def AutoLoadModel(self):
		self.LoadModel(self.Model)
	
	def SetInt(self,ModelConfigName,IncomingValue=None):
		self.ModelConfig[ModelConfigName] = None if IncomingValue is None else int(IncomingValue)
		
	def SetFloat(self,ModelConfigName,IncomingValue=None):
		self.ModelConfig[ModelConfigName] = None if IncomingValue is None else float(IncomingValue)
	
	def SetBool(self,ModelConfigName,IncomingValue=None):
		self.ModelConfig[ModelConfigName] = None if IncomingValue is None else bool(IncomingValue)
	
	def SetDoSample(self,IncomingValue=None):
		self.SetBool("do_sample",IncomingValue)
	
	def SetTemperature(self,IncomingValue=None):
				
		if IncomingValue is None:
			self.ModelConfig["temperature"] = None
		else:
			Temp = float(IncomingValue)
			self.ModelConfig["temperature"] = Temp if Temp > 0.0 else 0.01
	
	def SetTopK(self,IncomingValue=None):
		self.SetInt("top_k",IncomingValue)
	
	def SetTopP(self,IncomingValue=None):
		self.SetFloat("top_p",IncomingValue)
	
	def SetRepetitionPenalty(self,IncomingValue=None):
		self.SetFloat("repetition_penalty",IncomingValue)
	
	def SetBOSTokenID(self,IncomingValue=None):
		self.SetInt("bos_token_id",IncomingValue)
	
	def SetPADTokenID(self,IncomingValue=None):
		self.SetInt("pad_token_id",IncomingValue)
	
	def SetEOSTokenID(self,IncomingValue=None):
		self.SetInt("eos_token_id",IncomingValue)
	
	def SetLengthPenalty(self,IncomingValue=None):
		self.SetFloat("length_penalty",IncomingValue)
	
	def SetNumReturnSequences(self,IncomingValue=None):
		self.SetInt("num_return_sequences",IncomingValue)
	
	#MaxLength - The maximum length of the sequence to be generated						
	def CompleteText(self,*,TextPrompt,MaxLength):
		
		if not self.IsLoaded():
			self.AutoLoadModel()
		
		EncodedPrompt = self.Tokeniser.encode(TextPrompt, add_special_tokens = True)
		EncodedPrompt = tf.constant(EncodedPrompt, dtype = tf.int32)[None, :]
		
		OutputSequences = self.HeadModel.generate(	EncodedPrompt,
													do_sample = self.ModelConfig["do_sample"],
													max_length = int(MaxLength),
													temperature = self.ModelConfig["temperature"],
													top_k = self.ModelConfig["top_k"],
													top_p = self.ModelConfig["top_p"],
													repetition_penalty = self.ModelConfig["repetition_penalty"],
													bos_token_id=self.ModelConfig["bos_token_id"],
													pad_token_id=self.ModelConfig["pad_token_id"],
													eos_token_id=self.ModelConfig["eos_token_id"],
													length_penalty=self.ModelConfig["length_penalty"],
													num_return_sequences=self.ModelConfig["num_return_sequences"])
								
		return self.Tokeniser.decode(OutputSequences[0]).replace("<|endoftext|>","")
