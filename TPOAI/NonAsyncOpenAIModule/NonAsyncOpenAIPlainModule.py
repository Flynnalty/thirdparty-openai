"""
NonAsyncOpenAIPlain Module

Version 1.1
"""

import os
import openai #pip install openai
import json

CurrentDirectory = os.path.dirname(os.path.abspath(__file__))

class NonAsyncOpenAIPlain(object):
	
	OpenAIEngines = ["text-davinci-001",
					"text-curie-001",
					"text-babbage-001",
					"text-ada-001",
					"code-davinci-001",
					"code-cushman-001",
					"text-similarity-ada-001",
					"text-similarity-babbage-001",
					"text-similarity-curie-001",
					"text-similarity-davinci-001",
					"text-search-ada-doc-001",
					"text-search-ada-query-001",
					"text-search-babbage-doc-001",
					"text-search-babbage-query-001",
					"text-search-curie-doc-001",
					"text-search-curie-query-001",
					"code-search-ada-code-001",
					"code-search-ada-text-001",
					"code-search-babbage-code-001",
					"code-search-babbage-text-001"]
	
	ModelConfig = {
							"temperature":0.7, #0.0 to 1.0
							"top_p":0.0, #0.0 to 1.0
							"best_of":1, #1 to 20
							"frequency_penalty":0.0, #0.0 to 2.0
							"presence_penalty":0.0, #0.0 to 2.0
							"stop":[],#Array of strings
						}
	
	Engine = ""
	
	SafetyMode = True #Disable if you want unsafe content from the AI for debugging/testing purposes
	SensitivityThreshold = -0.355 #Sensitivity threshold based on OpenAI usage guidance page
	
	def __init__(self,LoadEngine=None):
		
		if LoadEngine is None:
			self.Engine = self.OpenAIEngines[0]
		
		else:
		
			LowerCase = str(LoadEngine).lower()
			self.Engine = LowerCase if LowerCase in self.OpenAIEngines else self.OpenAIEngines[0]
	
	def SetFloat(self,ModelConfigName,IncomingValue,*,Min=None,Max=None):
		
		if IncomingValue is None:
			self.ModelConfig[ModelConfigName] = None
		else:
			TempValue = float(IncomingValue)
			
			if not Min is None:
				if TempValue < Min:
					TempValue = Min
			elif not Max is None:
				if TempValue > Max:
					TempValue = Max
			
			self.ModelConfig[ModelConfigName] = float(TempValue)
	
	def SetInt(self,ModelConfigName,IncomingValue,*,Min=None,Max=None):
		
		if IncomingValue is None:
			self.ModelConfig[ModelConfigName] = None
		else:
			TempValue = int(IncomingValue)
			
			if not Min is None:
				if TempValue < Min:
					TempValue = Min
			elif not Max is None:
				if TempValue > Max:
					TempValue = Max
			
			self.ModelConfig[ModelConfigName] = int(TempValue)
	
	def SetTemperature(self,IncomingValue=None):
		self.SetFloat("temperature",IncomingValue,Min=0.0,Max=1.0)
	
	def SetBestOf(self,IncomingValue=None):
		self.SetInt("best_of",IncomingValue,Min=1,Max=20)
	
	def SetTopP(self,IncomingValue=None):
		self.SetFloat("top_p",IncomingValue,Min=0.0,Max=1.0)
	
	def SetFrequencyPenalty(self,IncomingValue=None):
		self.SetFloat("frequency_penalty",IncomingValue,Min=0.0,Max=2.0)
		
	def SetPresencePenalty(self,IncomingValue=None):
		self.SetFloat("presence_penalty",IncomingValue,Min=0.0,Max=2.0)
	
	def IsInAIEngineList(self,EngineNameToCheck):
		LowerCase = str(EngineNameToCheck).lower()
		return LowerCase in self.OpenAIEngines
	
	def SetAIEngine(self,NewEngine):
		
		LowerCase = str(NewEngine).lower()
		
		if LowerCase in self.OpenAIEngines:
			self.CurrentAIEngine = LowerCase
		else:
			raise ValueError('\''+str(NewEngine)+'\' is not a recognised AI engine.')

	def SafteyModeEnabled(self):
		self.SafetyMode = True
		
	def SafteyModeDisabled(self):
		self.SafetyMode = False

	def GetOpenAIClassifier(self,*,ResponseToClassify,UserID):
		return openai.Completion.create(engine="content-filter-alpha",
										prompt = "<|endoftext|>"+str(ResponseToClassify)+"\n--\nLabel:",
										temperature=0,
										max_tokens=1,
										top_p=0,
										logprobs=10,
										user=UserID)
	
	def GetOpenAIResponse(self,*,TokenLength,Text,UserID,AIEngine):
		
		Response = openai.Completion.create(engine=AIEngine, prompt=Text,
											max_tokens=int(TokenLength),
											temperature=0.7,
											presence_penalty=0.1,
											frequency_penalty=0.1,
											user=UserID)
		
		TempPythonObject = json.loads(str(Response))		
		return TempPythonObject["choices"][0]["text"]
		
	def ReplaceMultipleSpaces(self,TextToReplace):
		
		return re.sub(' +',' ',TextToReplace)
	
	#Propagate any changes to AsyncOpenAIPlainModule
	def ClassifyOpenAISentences(self,*,ResponseToFilter,UserID,Threshold):
		
		FilteredSpaces = self.ReplaceMultipleSpaces(ResponseToFilter)
		
		FilterText = FilteredSpaces.replace(". ",". <|es|>")
		FilterText = FilterText.replace("! ","! <|es|>")
		FilterText = FilterText.replace("? ","? <|es|>")
		
		EndSentenceRegex = re.compile(r'<\|es\|>')
		Sentences = re.split(EndSentenceRegex,FilterText)
		
		SentenceList = []
		
		for EachSentence in Sentences:
			
			Classifier = self.ClassifyOpenAIResponse(ResponseToClassify=EachSentence,UserID=UserID,Threshold=Threshold)
			
			SentenceList.append({
								"Text":EachSentence,
								"Classifier":Classifier,
								})
			
		ReturnValues = {
							"OriginalText":ResponseToFilter,
							"NormalisedText":FilteredSpaces,
							"TaggedText":FilterText,
							"Sentences":SentenceList,
						}
						
		return ReturnValues
	
	def FilterClassifiedSentences(self,ClassifiedSentences):
		
		TempOriginalText = ClassifiedSentences["NormalisedText"]
		
		for Each in ClassifiedSentences["Sentences"]:
			
			if Each["Classifier"] > 1:
				TempOriginalText = TempOriginalText.replace(Each["Text"],"")
			
		return TempOriginalText
		
	#Propagate any changes to AsyncOpenAIPlainModule
	def FilterOpenAISentences(self,*,ResponseText,UserID,Threshold):
		ClassifiedSentences = self.ClassifyOpenAISentences(ResponseToFilter=ResponseText,UserID=UserID,Threshold=Threshold)
		return self.FilterClassifiedSentences(ClassifiedSentences)
	
	def ClassifyReturnedResponse(self,*,ReturnedResponse,Threshold):
		
		#0 - likely safe
		#1 - sensitive
		#2 - NSFW or offensive (if it passes probability thresholds)
		
		OutputLabel = ReturnedResponse["choices"][0]["text"]
		
		if OutputLabel in ["0","1","2"]:
			ReturnClassifier = int(OutputLabel)
			Probability = ReturnedResponse["choices"][0]["logprobs"]["top_logprobs"][0]
			
			if OutputLabel == "2":
				
				TempThreshold = self.SensitivityThreshold if Threshold is None else Threshold
				
				if Probability["2"] < TempThreshold:
					Probability0 = Probability.get("0", None)
					Probability1 = Probability.get("1", None)

					if Probability0 is not None and Probability1 is not None:
						ReturnClassifier = 0 if (Probability0 >= Probability1) else 1
							
					else:
						ReturnClassifier = 0 if (Probability0 is not None) else 1
				
			return ReturnClassifier
		else:
			return 2
	
	#Propagate any changes to AsyncOpenAIPlainModule
	def ClassifyOpenAIResponse(self,*,ResponseToClassify,UserID,Threshold):
		return self.ClassifyReturnedResponse(ReturnedResponse=self.GetOpenAIClassifier(ResponseToClassify=ResponseToClassify,UserID=UserID),Threshold=Threshold)

	#Propagate any changes to AsyncOpenAIPlainModule
	def ProcessUserQuery(self,*,TotalTokenLength,Text,UserID,AIEngine=None):
		Engine = self.Engine if AIEngine is None or not self.IsInAIEngineList(AIEngine) else str(AIEngine).lower()
		
		Response = self.GetOpenAIResponse(TokenLength=TotalTokenLength,Text=Text,UserID=UserID,AIEngine=Engine)
		
		if self.SafetyMode:
			Classification = self.ClassifyOpenAIResponse(ResponseToClassify=Response,UserID=UserID,Threshold=self.SensitivityThreshold)
			
			if Classification == 2:
				return self.FilterOpenAISentences(ResponseToFilter=Response,UserID=UserID,Threshold=self.SensitivityThreshold)
		
		return Response
		
