"""
OpenAI Module

Version 1.2
"""

try:
	from .NonAsyncOpenAIPlainModule import *
except ImportError:
	from NonAsyncOpenAIPlainModule import *
except ModuleNotFoundError:
	from NonAsyncOpenAIPlainModule import *

import os
import openai #pip install openai
import json
from transformers import GPT2Tokenizer	#pip install transformers
										#pip install tensorflow

class NonAsyncOpenAI(NonAsyncOpenAIPlain):
	
	InternalTokeniser = None
	
	def __init__(self,LoadEngine=None):
		super().__init__(LoadEngine)
	
	def LoadTokeniser(self):
		#When this is called, GPT2's model will download automatically and install itself locally if it isn't already
		self.InternalTokeniser = GPT2Tokenizer.from_pretrained("gpt2")
	
	def CountTokens(self,IncomingText):
		
		#We wait until count tokens is actually called as it is extremely resource intensive to load the tokeniser
		#Users can call LoadTokeniser if they need it to fire off sooner
		if self.InternalTokeniser is None:
			self.LoadTokeniser()
		
		return len(self.InternalTokeniser(IncomingText)['input_ids'])
