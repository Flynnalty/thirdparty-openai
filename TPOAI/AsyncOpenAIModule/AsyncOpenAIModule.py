import os, sys, inspect

LocalFolder = os.path.realpath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())),".."))
if LocalFolder not in sys.path:
	sys.path.insert(0, LocalFolder)

from NonAsyncOpenAIModule import *

try:
	from .AsyncOpenAIPlainModule import *
except ImportError:
	from AsyncOpenAIPlainModule import *
except ModuleNotFoundError:
	from AsyncOpenAIPlainModule import *

import asyncio
import openai #pip install openai
import json
from transformers import GPT2Tokenizer	#pip install transformers
										#pip install tensorflow

class AsyncOpenAI(AsyncOpenAIPlain,NonAsyncOpenAI):
	
	InternalTokeniser = None
	
	def __init__(self,LoadEngine=None):
		super().__init__(LoadEngine)
	
	async def LoadTokeniser(self):
		super().LoadTokeniser()
	
	async def CountTokens(self,IncomingText):
		
		#We wait until count tokens is actually called as it is extremely resource intensive to load the tokeniser
		#Users can call LoadTokeniser if they need it to fire off sooner
		if self.InternalTokeniser is None:
			await self.LoadTokeniser()
		
		return len(self.InternalTokeniser(IncomingText)['input_ids'])


if __name__ == "__main__":
	Test = AsyncOpenAI()
