
import asyncio
import os, sys, inspect

LocalFolder = os.path.realpath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())),".."))
if LocalFolder not in sys.path:
	sys.path.insert(0, LocalFolder)

from NonAsyncOpenAIModule import *

class AsyncGPT2(NonAsyncGPT2):
	
	def __init__(self,LoadModel="gpt2"):
		super().init(LoadModel)
	
	async def IsLoaded(self):
		return super().IsLoaded()
		
	async def LoadTokeniser(self,Model):
		super().LoadTokeniser(Model)
		
	async def LoadHeadModel(self,Model):
		super().LoadHeadModel(Model)
		
	async def LoadModel(self,Model):
		super().LoadModel(Model)

	async def AutoLoadModel(self):
		super().AutoLoadModel(self.Model)
	
	async def SetDoSample(self,IncomingValue):
		super().SetDoSample(IncomingValue)
	
	async def SetTemperature(self,IncomingValue):
		super().SetTemperature(IncomingValue)
	
	async def SetTopK(self,IncomingValue):
		super().SetTopK(IncomingValue)
	
	async def SetTopP(self,IncomingValue):
		super().SetTopP(IncomingValue)
	
	async def SetRepetitionPenalty(self,IncomingValue):
		super().SetRepetitionPenalty(IncomingValue)
	
	async def SetBOSTokenID(self,IncomingValue):
		super().SetBOSTokenID(IncomingValue)
	
	async def SetPADTokenID(self,IncomingValue):
		super().SetPADTokenID(IncomingValue)
	
	async def SetEOSTokenID(self,IncomingValue):
		super().SetEOSTokenID(IncomingValue)
	
	async def SetLengthPenalty(self,IncomingValue):
		super().SetLengthPenalty(IncomingValue)
	
	async def SetNumReturnSequences(self,IncomingValue):
		super().SetNumReturnSequences(IncomingValue)
	
	#MaxLength - The maximum length of the sequence to be generated						
	async def CompleteText(self,*,TextPrompt,MaxLength):
		
		return super().CompleteText(TextPrompt=TextPrompt,MaxLength=MaxLength)

