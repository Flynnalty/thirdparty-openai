import asyncio
import os, sys, inspect

LocalFolder = os.path.realpath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())),".."))
if LocalFolder not in sys.path:
	sys.path.insert(0, LocalFolder)

from NonAsyncOpenAIModule import *

class AsyncOpenAIPlain(NonAsyncOpenAIPlain):
	
	def __init__(self,LoadEngine):
		super().__init__(LoadEngine)
	
	async def SetFloat(self,ModelConfigName,IncomingValue,*,Min=None,Max=None):
		super().SetFloat(ModelConfigName,IncomingValue,Min=Min,Max=Max)
	
	async def SetInt(self,ModelConfigName,IncomingValue,*,Min=None,Max=None):
		super().SetInt(ModelConfigName,IncomingValue,Min=Min,Max=Max)
	
	async def SetTemperature(self,IncomingValue=None):
		super().SetTemperature(IncomingValue)
	
	async def SetBestOf(self,IncomingValue=None):
		super().SetBestOf(IncomingValue)
	
	async def SetTopP(self,IncomingValue=None):
		super().SetTopP(IncomingValue)
	
	async def SetFrequencyPenalty(self,IncomingValue=None):
		super().SetFrequencyPenalty(IncomingValue)
		
	async def SetPresencePenalty(self,IncomingValue=None):
		super().SetPresencePenalty(IncomingValue)
	
	async def IsInAIEngineList(self,EngineNameToCheck):
		return super().IsInAIEngineList(EngineNameToCheck)
	
	async def SetAIEngine(self,NewEngine):
		super().SetAIEngine(NewEngine)

	async def SafteyModeEnabled(self):
		super().SafteyModeEnabled()
		
	async def SafteyModeDisabled(self):
		super().SafteyModeDisabled()

	async def GetOpenAIClassifier(self,*,ResponseToClassify,UserID):
		super().GetOpenAIClassifier(ResponseToClassify=ResponseToClassify,UserID=UserID)
	
	async def GetOpenAIResponse(self,*,TokenLength,Text,UserID,AIEngine):
		super().GetOpenAIResponse(TokenLength=TokenLength,Text=Text,UserID=UserID,AIEngine=AIEngine)
		
	async def ReplaceMultipleSpaces(self,TextToReplace):
		return super().ReplaceMultipleSpaces
	
	#Incomplete, needs to be added to all other functions
	async def ClassifyOpenAISentences(self,*,ResponseToFilter,UserID,Threshold):
		
		FilteredSpaces = await self.ReplaceMultipleSpaces(ResponseToFilter)
		
		FilterText = FilteredSpaces.replace(". ",". <|es|>")
		FilterText = FilterText.replace("! ","! <|es|>")
		FilterText = FilterText.replace("? ","? <|es|>")
		
		EndSentenceRegex = re.compile(r'<\|es\|>')
		Sentences = re.split(EndSentenceRegex,FilterText)
		
		SentenceList = []
		
		for EachSentence in Sentences:
			
			Classifier = await self.ClassifyOpenAIResponse(ResponseToClassify=EachSentence,UserID=UserID,Threshold=Threshold)
			
			SentenceList.append({
								"Text":EachSentence,
								"Classifier":Classifier,
								})
			
		ReturnValues = {
							"OriginalText":ResponseToFilter,
							"NormalisedText":FilteredSpaces,
							"TaggedText":FilterText,
							"Sentences":SentenceList,
						}
						
		return ReturnValues
	
	async def FilterClassifiedSentences(self,ClassifiedSentences):
		return super().FilterClassifiedSentences
		
	async def FilterOpenAISentences(self,*,ResponseText,UserID,Threshold):
		ClassifiedSentences = await self.ClassifyOpenAISentences(ResponseToFilter=ResponseText,UserID=UserID,Threshold=Threshold)
		return await self.FilterClassifiedSentences(ClassifiedSentences)
	
	async def ClassifyReturnedResponse(self,ReturnedResponse):
		return super().ClassifyReturnedResponse
	
	async def ClassifyOpenAIResponse(self,*,ResponseToClassify,UserID,Threshold):
		return await self.ClassifyReturnedResponse(ReturnedResponse=await self.GetOpenAIClassifier(ResponseToClassify,UserID),Threshold=Threshold)

	async def ProcessUserQuery(self,*,TotalTokenLength,Text,UserID,AIEngine=None):
		
		Engine = self.Engine if AIEngine is None or not self.IsInAIEngineList(AIEngine) else str(AIEngine).lower()
		
		Response = await self.GetOpenAIResponse(TokenLength=TotalTokenLength,Text=Text,UserID=UserID,AIEngine=Engine)
		
		if self.SafetyMode:
			Classification = await self.ClassifyOpenAIResponse(ResponseToClassify=Response,UserID=UserID,Threshold=self.SensitivityThreshold)
			
			if Classification == 2:
				return await self.FilterOpenAISentences(ResponseToFilter=Response,UserID=UserID,Threshold=self.SensitivityThreshold)
		
		return Response
