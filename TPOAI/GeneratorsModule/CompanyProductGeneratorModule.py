"""
CompanyProductGenerator

"""


try:
	from PromptGeneratorModule import *
except ImportError:
	from .PromptGeneratorModule import *
except ModuleNotFoundError:
	from .PromptGeneratorModule import *

class CompanyProductGenerator(PromptGenerator):
		
	def __init__(self,**kwargs):
		
		
		TempDict = {
					"WritingStyles":["polite"],
					"WritingGenres":["informational","serious"],
					"MessageFormats":[
										"corporate message",
										"advertisement",
										"corporate email",
										"flier",
										"informational",
										"business card",
										"promotional",
										"merchanise store"
										],
					}
					
		self.SetPrompts(PromptsDictionary=TempDict,XORMode=True)
		super().__init__(**kwargs)
