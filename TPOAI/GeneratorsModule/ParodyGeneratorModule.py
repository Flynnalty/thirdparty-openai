"""
ParodyGenerator

"""


try:
	from CompanyProductGeneratorModule import *
except ImportError:
	from .CompanyProductGeneratorModule import *
except ModuleNotFoundError:
	from .CompanyProductGeneratorModule import *

class ParodyGenerator(CompanyProductGenerator):	
	
	def __init__(self,**kwargs):
		
		TempDict = {
					"WritingStyles":["direct", "indirect", "pastiche"],
					"WritingGenres":["parody","absurdist humour","dark humour","satire", "wit", "dry humour"],
					}
					
		self.SetPrompts(PromptsDictionary=TempDict,XORMode=True)
		super().__init__(**kwargs)
