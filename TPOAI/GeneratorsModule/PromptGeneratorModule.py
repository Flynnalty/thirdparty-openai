"""
PromptGenerator
"""
import random

class PromptGenerator(object):
	
	Prompts = {}
	
	def __init__(self,**kwargs):
		
		for EachKey, EachValue in kwargs.items():
			self.SetPrompt(PromptKey=EachKey,PromptValue=EachValue)
	
	def SetPrompts(self,*,PromptsDictionary,XORMode=False):
		for EachKey, EachValue in PromptsDictionary.items():
			self.SetPrompt(PromptKey=EachKey,PromptValue=EachValue,XORMode=XORMode)
	
	#Prompt: Can be string or list
	def SetPrompt(self,*,PromptKey,PromptValue,XORMode=False):
		
		if XORMode:			
			if not PromptKey in self.Prompts:
				self.Prompts[PromptKey] = PromptValue
			
		else:
			self.Prompts[PromptKey] = PromptValue
	
	def GetPrompt(self,PromptName):
		return self.Prompts[PromptName]
	
	def GeneratePromptFromDictionary(self,*,Dictionary,RandomiseLists=True):
		TextPrompt = ""
		
		for EachPrompt in Dictionary:
			
			if type(Dictionary[EachPrompt]) is list:
				if len(Dictionary[EachPrompt]) < 1:
					continue
				
				elif RandomiseLists:
					TextPrompt += str(EachPrompt)+": "+Dictionary[EachPrompt][random.randint(0,len(Dictionary[EachPrompt])-1)]+"\n"
				else:
					TextPrompt += str(EachPrompt)+": "
					
					for EachItem in Dictionary[EachPrompt]:
						TextPrompt += str(EachItem) + ", "
						
					TextPrompt = TextPrompt[:-2] + "\n"
			else:
				
				if Dictionary[EachPrompt] is None or str(Dictionary[EachPrompt]) == "":
					continue
				else:
					TextPrompt += str(EachPrompt)+": "+str(Dictionary[EachPrompt])+"\n"
			
		return TextPrompt
	
	def GeneratePrompt(self,*,RandomiseLists=True):
		return self.GeneratePromptFromDictionary(Dictionary=self.Prompts,RandomiseLists=RandomiseLists)

	def GeneratePromptDynamic(self,*,TempPrompts={},RandomiseLists=True):
		TextPrompt = self.GeneratePrompt(RandomiseLists=RandomiseLists)
		return TextPrompt + self.GeneratePromptFromDictionary(Dictionary=TempPrompts,RandomiseLists=RandomiseLists)
		
