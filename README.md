# ThirdPartyOpenAI (TPOAI)

A set of unofficial third party Python libraries for use with OpenAI API.

# Disclaimer
Code is provided as-is with no warranty or merchantability of fitness. Users assume all liability of code.

# Licence
Code is licenced under the MIT licence.

# Requirements

# Requirements for GPT-2

[Optional: Only if GPT-2 is used for TokenCounts or GPT-2 Completion]

* Minimum 1GB free harddrive space [more for larger models]
* Good download speed [downloads model locally]
* Minimum 8GB RAM [more for larger models]
* GPU [optional but recommended]
* Decent CPU [more processing power for larger models]

Notes:
* Download occurs once, automatically, when your code calling it is run.
* Does not need to be re-downloaded on follow up runs, if completed successfully, except on a new machine or different model.
* Prints excessive CUDA warnings on devices without a GPU (can be ignored)
* Recommended to use far fewer tokens on older machines
* May crash if model size is too big for the computer running it to handle
* Not required to use OpenAI classes features, except CountTokens
* Required for CountTokens (only initialised/downloaded if CountTokens or LoadTokeniser is called)

# Requirements for PIP

Must be Python 3.
Code requires the following installed (pip3 users can replace pip with pip3):
* pip install asyncio
* pip install openai
* pip install tensorflow
* pip install transformers

# Example Prompt Generator and OpenAI Call

`#

	import os
	import openai
	from TPOAI import *

    #Override with your own custom prompt generator
	class WindowCleanGenerator(CompanyProductGenerator):	
		
		def __init__(self,**kwargs):
			
			TempDict = {
						"CompanyName":"My Company",
						"Setting":["corporate"],
						"Products":["shoes"],
						"Services":["window cleaning"],
						}
						
			self.SetPrompts(PromptsDictionary=TempDict,XORMode=True)
			super().__init__(**kwargs)

	if __name__ == '__main__':
		
		openai.api_key = "YOUR_API_KEY"
		NAOAIP = NonAsyncOpenAIPlain("text-davinci-001") #Optional. Defaults to text-davinci-001
		
		WCG = WindowCleanGenerator()
		CurrentTopics = {"Topics":["Summer","Heatwave"]} #Can be an empty dictionary
		
		TextPrompt = WCG.GeneratePromptDynamic(TempPrompts=CurrentTopics,RandomiseLists=False)
		Response = NAOAIP.ProcessUserQuery(TotalTokenLength=300,Text=TextPrompt,UserID="UserID Goes Here")
		
		print(Response)

`

# Example GPT-2 Code To Complete Text

`
#

	import os
	from TPOAI import *

	if __name__ == '__main__':
		
		#Note, CUDA is quite 'talkative' code wise and will produce a lot of unwanted printouts
		#This cannot be suppressed. However, it should not impact how code performs.
		
		#Note the absence of any API key requirements
		
		#Defaults to "gpt2" if no argument supplied
		NAGPT2 = NonAsyncGPT2("gpt2") #There's also gpt2-medium, gpt2-large and gpt2-xl
		#Optional modifier argument. Note, argument names are NOT always the same as the OpenAI API calls.
		NAGPT2.SetTemperature(0.7)
		
		#Note the arguments are the other way around to the OpenAI calls
		#Shorter lengths are advised on older machines
		print(NAGPT2.CompleteText(TextPrompt="Testing",MaxLength=50)) #Automatically downloads the model if not already installed at this point
`
